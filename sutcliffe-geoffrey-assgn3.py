#!/usr/bin/env python3

import argparse
import sys
import os
import re
import unicodedata
import codecs
import math
import random
import statistics
import nltk
# from nltk.tag.perceptron import PerceptronTagger

# tagger = PerceptronTagger()
from collections import Counter
import numpy as np
import matplotlib.pyplot as pyplot
from enum import Enum
from sklearn import svm
import time
from string import punctuation
from collections import defaultdict
from sklearn import preprocessing

tagger = nltk.data.load(nltk.tag._POS_TAGGER)

import pickle
from nltk.collocations import *
from sklearn.linear_model import SGDClassifier


# -try adding in stemming
# -do simple naive bayes [done]
# -compare performance with binary multinomial naive Bayes
# -simple sentiment modulation for negation. - also add  neg to stop words - \b(?:not|never|no|n\'t)\b[\w\s]+[^\w\s] substitute NOT
# -sentiment lexicons - sentiment symposium
# parameterize laplacesmoothing
# interesting observation made after baseline implementation - doc 1106 is wrong every time.

def main(argv):
    global debug_traces
    bAssessPerformance = True
    nTrainingFolds = 10
    fRatioHoldOverForTest = 0.3

    # if the test filename is not provided, a test set will be held over from the training set
    strTestFilename = ""
    bTestSetProvided = False

    strStopFilename = ""

    strPosFilenameTrain = "pos_count_train.p"
    strPosFilenameTest = "pos_count_test.p"
    bUsePosFile = False
    bCreatePosFile = False

    bUseNbClassifier = False

    strNegativeSentimentWords = "negative-words.txt"
    strPositiveSentimentWords = "positive-words.txt"

    parser = argparse.ArgumentParser(description="Deception detection Cmdline Parser")
    parser.add_argument('-t', '--truthful_training_set', help='Truthful file name', required=True)
    parser.add_argument('-f', '--falsified_training_set', help='Falsified file name', required=True)
    parser.add_argument('-x', '--x_validation_folds', help='[Optional] Number of X-validation training folds',
                        required=False, default=nTrainingFolds)
    parser.add_argument('-j', '--test_ratio', help='[Optional] ratio of training set to hold over for test',
                        required=False, default=fRatioHoldOverForTest)
    parser.add_argument('-tt', '--test_set', help='[Optional] Test file name', required=False)
    parser.add_argument('-d', '--debug', help='[Optional] Debug traces?', action="store_true", required=False)
    parser.add_argument('-s', '--stopword', help='[Optional] Stopword List', required=False)
    parser.add_argument('-p', '--use_pos_file', help='[Optional] Use Parts of Speech file (pos.p)', action="store_true", required=False)
    parser.add_argument('-c', '--create_pos_file', help='[Optional] Create Parts of Speech file (pos.p)',
                        action="store_true", required=False)
    parser.add_argument('-nb', '--use_nb_classifier', help='[Optional] Use old NB classifier for either DEV or TEST',
                        action="store_true", required=False)

    args = parser.parse_args()

    if args.test_ratio:
        fRatioHoldOverForTest = float(args.test_ratio)
    if args.x_validation_folds:
        nTrainingFolds = int(args.x_validation_folds)
    if args.test_set:
        bTestSetProvided = True
        strTestFilename = args.test_set

    if args.stopword:
        strStopFilename = args.stopword

    if args.use_pos_file:
        bUsePosFile = True

    if args.create_pos_file:
        bCreatePosFile = True

    if args.use_nb_classifier:
        bUseNbClassifier = True

    debug_traces = args.debug

    strTruthfulFilename = args.truthful_training_set
    strFalsifiedFilename = args.falsified_training_set
    strOutputFilename = "sutcliffe-geoffrey-assgn3-out.txt"

    print('###################')
    print("Truthful Examples file: ", strTruthfulFilename)
    print("Falsified Examples file: ", strFalsifiedFilename)

    if bTestSetProvided:
        # Do not do cross validation - use the entire training set
        print("Test training set provided:", strTestFilename)
        print("Will use all training data for... training")
    else:
        # Perform cross validation
        print("Test set will be held over from training set...")
        print("   Number of training folds: ", nTrainingFolds)
        print("   Ratio of training set to hold out for training: ", fRatioHoldOverForTest)
        # should make sure that nTrainingFolds is not used if the test set is provided!
    print("Create Parts of Speech file:", bCreatePosFile)
    print("Use Parts of Speech file:", bUsePosFile)
    print("Debug traces?: ", debug_traces)
    print("Use old implementation?:", bUseNbClassifier)
    print('###################')

    conStopwords = ReadStopWords(strStopFilename)
    conNegativeSentimentWords = ExtractSentimentWords(strNegativeSentimentWords)
    conPositiveSentimentWords = ExtractSentimentWords(strPositiveSentimentWords)

    # conTestSet = {}  # {test_doc_id -> test doc}
    # conTrainingSet = {}  # {training_doc_id -> class}
    # conLabels = {}  # {training_doc_id -> class}
    # conDocs = {}  # {training_doc_id -> training_doc}

    conDocs, conDocsProcessed, conLabels = ExtractDocuments(strTruthfulFilename, strFalsifiedFilename, conStopwords)

    conTestSet = {}
    conTestSetProcessed = {}

    # All of this application-launch logic needs a lot of refactoring. It's a mess!
    if bTestSetProvided:
        # Get the pure and preprocessed test set documents
        conTestSet, conTestSetProcessed = ReadTestSet(strTestFilename, conStopwords)

    if bCreatePosFile:
        print("Analysing POS in all training data - may take a while...")
        conPOSTrainOutput = CountPOS(conDocs)
        if bTestSetProvided:
            print("Analysing POS in all test data - may take a while...")
            conPOSTestOutput = CountPOS(conTestSet)

        print("Writing training POS to file")
        pickle.dump(conPOSTrainOutput, open(strPosFilenameTrain, "wb"))
        if bTestSetProvided:
            print("Writing test POS to file")
            pickle.dump(conPOSTestOutput, open(strPosFilenameTest, "wb"))
        print("POS files created")

    else:
        conPosTrainingSet = {}
        if not bUseNbClassifier:
            if bUsePosFile:
                print("Using training data POS from", strPosFilenameTrain)
                conPOSTrainingSet = pickle.load(open(strPosFilenameTrain, "rb"))

            else:
                conPOSTrainingSet = CountPOS(conDocs)

        if bTestSetProvided:  # need to adapt to use POS file
            # use the full set of docs for training
            if bUseNbClassifier:
                conTrainingSet = conDocsProcessed

                conVocab, llp, lln = TrainNB(conDocsProcessed, conTrainingSet, conLabels)
                with open(strOutputFilename, 'w') as f:
                    for doc in conTestSetProcessed:
                        classification, ratio = Classify(conVocab, llp, lln, conTestSetProcessed[doc])
                        print(doc, "\t", classification.name)
                        f.write(doc + "\t" + classification.name + "\n")
                    f.close()
            else:
                if bUsePosFile:
                    print("Using test data POS from", strPosFilenameTest)
                    conPOSTestSet = pickle.load(open(strPosFilenameTest, "rb"))

                    print("Extracting features from training set")
                    conVocab = GetVocab(conDocs, conDocsProcessed)
                    conBigrams = None
                    conFeaturesTrain = ExtractFeatures(conDocs, conPOSTrainingSet, conDocs, conDocsProcessed, conPositiveSentimentWords, conNegativeSentimentWords, conVocab, conBigrams)
                    conCoupled = CoupleDataAndTarget(conFeaturesTrain, conLabels)
                    X = [x[0] for x in conCoupled.values()]
                    Y = [x[1].value for x in conCoupled.values()]
                    print("Training discriminator")
                    clf = svm.SVC(kernel='rbf', C=1)#, gamma=0.12, probability=True)  # should be poly based on Shojaee2013
                    X_norm = preprocessing.normalize(X)
                    scaler = preprocessing.StandardScaler().fit(X)
                    X_scaled = scaler.transform(X)
                    clf.fit(X_scaled, Y)

                    print("******* Testing trained model on test set ******")
                    conFeaturesTest = ExtractFeatures(conTestSet, conPOSTestSet, conTestSet, conTestSetProcessed, conPositiveSentimentWords, conNegativeSentimentWords, conVocab, conBigrams)
                    #Using None is hacky, but its due in a couple of hours, so...
                    conCoupled = CoupleDataAndTarget(conFeaturesTest, None)
                    X = [x[0] for x in conCoupled.values()]
                    #Y = [x[1].value for x in conCoupled.values()]
                    doc_ids = [x for x in conCoupled.keys()]
                    X_norm = preprocessing.normalize(X)
                    X_scaled = scaler.transform(X)
                    # for doc in conTestSet:
                    # classification, ratio = Classify(conVocab, llp, lln, conDocs[doc])
#                    for doc_id in conTestSet:
                    with open(strOutputFilename, 'w') as f:
                        for idx, doc_id in enumerate(doc_ids):
                            classification = Classification(clf.predict(X_scaled[idx]))
                            print(doc_id, "\t", classification.name)
                            f.write(doc_id + "\t" + classification.name + "\n")
                    f.close()

        else:
            # we need to loop and split the training set a different way each time
            # an nTrainingFolds list of test/training sets
            executions = SplitTrainingSet(conDocs, conLabels, nTrainingFolds, fRatioHoldOverForTest)
            conScores = []

            #Lazy just do it all for new or old algo
            if bUseNbClassifier:
                with open(strOutputFilename, 'w') as f:
                    f.write("Doc\t\t\tClass\tCorrect?\n----------------------------")
                    for execution in enumerate(executions):
                        exec_num = str(execution[0])
                        print("\n\n******* Execution", exec_num, "*******")

                        f.write("\nFor execution " + exec_num + "\n" )
                        print("******* Training for execution", execution[0], "*******")
                        conVocab, llp, lln = TrainNB(conDocs, execution[1][1], conLabels)
                        print("******* Testing execution", execution[0], "*******")
                        nCountCorrect = 0
                        conTestSet = execution[1][0]
                        for doc in conTestSet:
                            classification, ratio = Classify(conVocab, llp, lln, conDocs[doc])
                            actual_classification = conLabels[doc]
                            if classification == actual_classification:
                                nCountCorrect = nCountCorrect + 1
                            if debug_traces:
                                print(doc, classification.name, ", +/- classification ratio:", ratio , ", Correct?:", classification == actual_classification)

                            strOutput = " \t".join([doc, classification.name, str(classification == actual_classification)])
                            f.write(strOutput + '\n')
                        fScore = nCountCorrect / len(conTestSet)
                        print("Ratio correct:",fScore)
                        conScores.append(fScore)
                    f.close()



                fMeanScore = statistics.mean(conScores)
                fVariance = statistics.pvariance(conScores)
                # This retrieves the index of the worst score, so we can retrieve it and study it.
                fWorstScore, nWorstScoreIndex = min((fWorstScore, nWorstScoreIndex) for (nWorstScoreIndex, fWorstScore) in enumerate(conScores))
                fBestScore, nBestScoreIndex = max((fBestScore, nBestScoreIndex) for (nBestScoreIndex, fBestScore) in enumerate(conScores))
                conWorstTestSet = executions[nWorstScoreIndex][0]
                conBestTestSet = executions[nBestScoreIndex][0]
                print("\nMean score:", fMeanScore, "Variance:", fVariance, ", Worst Score:", fWorstScore, "for test set", nWorstScoreIndex, ", Best Score:", fBestScore, "for test set", nBestScoreIndex)
                print("Check output file for analysis of this set")
            else:
                with open(strOutputFilename, 'w') as f:
                    f.write("Doc\t\t\tClass\tCorrect?\n----------------------------")
                    for execution in enumerate(executions):
                        exec_num = str(execution[0])
                        print("\n\n******* Execution", exec_num, "*******")

                        f.write("\nFor execution " + exec_num + "\n")
                        print("******* Training for execution", execution[0], "*******")

                        print("Extracting features")
                        conVocab = GetVocab(execution[1][1], conDocsProcessed)
                        conBigrams = None
                        conFeaturesTrain = ExtractFeatures(execution[1][1], conPOSTrainingSet, conDocs, conDocsProcessed, conPositiveSentimentWords, conNegativeSentimentWords, conVocab, conBigrams)
                        conCoupled = CoupleDataAndTarget(conFeaturesTrain, conLabels)
                        X = [x[0] for x in conCoupled.values()]
                        Y = [x[1].value for x in conCoupled.values()]
                        print("Training discriminator")
                        clf = svm.SVC(kernel='rbf', C=1)#, gamma=0.12, probability=True)  # should be poly based on Shojaee2013
                        X_norm = preprocessing.normalize(X)
                        scaler = preprocessing.StandardScaler().fit(X)
                        X_scaled = scaler.transform(X)
                        clf.fit(X_scaled, Y)

                        # conVocab, llp, lln = TrainNB(conDocs, execution[1][1], conLabels)
                        print("******* Testing execution", execution[0], "*******")
                        nCountCorrect = 0
                        conTestSet = execution[1][0]
                        conFeaturesTest = ExtractFeatures(conTestSet, conPOSTrainingSet, conDocs, conDocsProcessed, conPositiveSentimentWords, conNegativeSentimentWords, conVocab, conBigrams)
                        conCoupled = CoupleDataAndTarget(conFeaturesTest, conLabels)
                        X = [x[0] for x in conCoupled.values()]
                        Y = [x[1].value for x in conCoupled.values()]
                        doc_ids = [x for x in conCoupled.keys()]
                        X_norm = preprocessing.normalize(X)
                        X_scaled = scaler.transform(X)
                        # for doc in conTestSet:
                        # classification, ratio = Classify(conVocab, llp, lln, conDocs[doc])
    #                    for doc_id in conTestSet:
                        for idx, doc_id in enumerate(doc_ids):
                            classification = Classification(clf.predict(X_scaled[idx]))
                            actual_classification = Classification(Y[idx])
                            if classification == actual_classification:
                                nCountCorrect += 1
                            if debug_traces:
                                print(doc_id, classification.name, "Correct?:",
                                      classification == actual_classification)

                            strOutput = " \t".join(
                                [doc_id, classification.name, str(classification == actual_classification)])
                            f.write(strOutput + '\n')
                        fScore = nCountCorrect / len(conTestSet)
                        print("Ratio correct:", nCountCorrect, "/", len(conTestSet), fScore)
                        conScores.append(fScore)
                    f.close()

                fMeanScore = statistics.mean(conScores)
                fVariance = statistics.pvariance(conScores)
                # This retrieves the index of the worst score, so we can retrieve it and study it.
                fWorstScore, nWorstScoreIndex = min(
                    (fWorstScore, nWorstScoreIndex) for (nWorstScoreIndex, fWorstScore) in enumerate(conScores))
                fBestScore, nBestScoreIndex = max(
                    (fBestScore, nBestScoreIndex) for (nBestScoreIndex, fBestScore) in enumerate(conScores))
                conWorstTestSet = executions[nWorstScoreIndex][0]
                conBestTestSet = executions[nBestScoreIndex][0]
                print("\nMean score:", fMeanScore, "Variance:", fVariance, ", Worst Score:", fWorstScore, "for test set",
                      nWorstScoreIndex, ", Best Score:", fBestScore, "for test set", nBestScoreIndex)
                print("Check output file for analysis of this set")

def ExtractFeatures(_conDocsToProcess, _conPOS, _conDocs, _conDocsPreProcessed, _conPositiveSentimentWords, _conNegativeSentimentWords, _conVocabProcessed, _conBigrams):
    conFeatures = {}
    # nltk.help.upenn_tagset()
    for doc_id in _conDocsToProcess:
        nVerb = _conPOS[doc_id]['VB'] #+ _conPOS[doc_id]['VBG'] #+ _conPOS[doc_id]['VBG'] #+ _conPOS[doc_id]['VBP'] + _conPOS[doc_id]['VBZ']
        nVerbPastTense = _conPOS[doc_id]['VBD'] + _conPOS[doc_id]['VBN']
        nPronoun = _conPOS[doc_id]['PRP'] + _conPOS[doc_id]['PRP$']
        nNoun = _conPOS[doc_id]['NN'] + _conPOS[doc_id]['NNS'] #+ _conPOS[doc_id]['NNP'] + _conPOS[doc_id]['NNPS']
        nProperNoun = _conPOS[doc_id]['NNP'] + _conPOS[doc_id]['NNPS']
        nAdjective = _conPOS[doc_id]['JJ'] + _conPOS[doc_id]['JJR'] + _conPOS[doc_id]['JJS']
        nDeterminer = _conPOS[doc_id]['PDT'] + _conPOS[doc_id]['WDT'] + _conPOS[doc_id]['DT']
        nAdverb = _conPOS[doc_id]['RB'] + _conPOS[doc_id]['RBR'] + _conPOS[doc_id]['RBS']
        nPreposition = _conPOS[doc_id]['IN']

        nVerbRatio = nVerb / sum(_conPOS[doc_id].values())
        nVerbPastTenseRatio = nVerbPastTense / sum(_conPOS[doc_id].values())
        nPronounRatio = nPronoun / sum(_conPOS[doc_id].values())
        nNounRatio = nNoun / sum(_conPOS[doc_id].values())
        nProperNounRatio = nProperNoun / sum(_conPOS[doc_id].values())
        nAdjectiveRatio = nAdjective / sum(_conPOS[doc_id].values())
        nDeterminerRatio = nDeterminer / sum(_conPOS[doc_id].values())
        nAdverbRatio = nAdverb / sum(_conPOS[doc_id].values())
        nPrepositionRatio = nPreposition / sum(_conPOS[doc_id].values())

        #features = FeatureSet()
        features = []
        # features.append(nVerbRatio)
        features.append(nVerbPastTense)
        features.append(nPronoun)
        #features.append(nNoun)
        #features.append(nProperNounRatio)
        features.append(nAdjective)
        #features.append(nDeterminerRatio)
        # features.append(nAdverbRatio)
        # features.append(nPrepositionRatio)

        #Ratio of parts of speech
        #features.append(len(_conPOS[doc_id]) / len(_conDocs[doc_id]))


        #Richness of words? features.append(len(_conDocs[doc_id]) / len(set(_conDocs[doc_id])))



        nPositiveSentimentWords = 0
        nNegativeSentimentWords = 0
        nPuncts = 0
        nChars = 0
        nDigits = 0
        nAlphas = 0
        nUppers = 0
        nSpaces = 0
        nShortWords = 0
        nLegomena = 0
        nDislegomena = 0

        countWords = {}
        preProcessedDocument = _conDocsPreProcessed[doc_id]
        pure_document = _conDocs[doc_id]

        nChars = len(pure_document)

        words = pure_document.split()
        nWords = len(words)
        conWordTypes = set(words)
        conWordLengths = Counter([len(word) for word in words])
        #conWordLengths.setdefault(0)

        for word in words:
            for ch in word:
                if ch in punctuation:
                    nPuncts += 1
                elif ch.isspace():
                    nSpaces += 1
                elif ch.isupper():
                    nUppers += 1

        words = preProcessedDocument.split()
        for word in words:
            for ch in word:
                nChars += 1
                if ch.isdigit():
                    nDigits += 1
                elif ch.isalpha():
                    nAlphas += 1



            if word in _conPositiveSentimentWords:
                nPositiveSentimentWords += 1
            if word in _conNegativeSentimentWords:
                nNegativeSentimentWords += 1
            if len(word) < 4:
                nShortWords += 1

            if _conVocabProcessed[word] == 1:
                nLegomena += 1
            elif _conVocabProcessed[word] == 2:
                nDislegomena += 1
        #
        # features.append(nChars)
        #features.append(nDigits / nChars)
        # features.append(nAlphas / nChars)
        #features.append(nUppers / nChars)
        # features.append(nSpaces / nChars)




        #Unigrams
        # for word in _conVocab:
        #     if word in words:
        #         features.append(1)
        #     else:
        #         features.append(0)

        #Bigrams
        # finder = BigramCollocationFinder.from_words(words)
        # conNgrams = finder.ngram_fd
        # for ngram in _conBigrams:
        #     if ngram in conNgrams:
        #         features.append(1)
        #     else:
        #         features.append(0)

        #Word based features
        #features.append(nWords)

        #fSentimentWordRatio = nSentimentWords / len(_conDocs[doc_id])
        #features.append(fSentimentWordRatio)
        features.append(nPositiveSentimentWords)
        #features.append(nNegativeSentimentWords)

        #sentences = nltk.sent_tokenize(_conDocs[doc_id])
        #features.append(len(words) / len(sentences))
        #features.append(nShortWords/nWords)
        # for n in range(1,16):
        #     features.append(conWordLengths[n])

        fMeanWordLength = sum(len(word) for word in words) / len(words)
        # features.append(fMeanWordLength)
        nWordTypeRatio = len(conWordTypes) / nWords
        #features.append(nWordTypeRatio)

        features.append(nPuncts)

        nYuleValue = 1/yule(_conDocs[doc_id])
        #features.append(nYuleValue)
        #features.append(nDislegomena)
        features.append(nLegomena/ nWords)

        conFeatures[doc_id] = features

    return conFeatures

# Make sure data and results are in the same order
def CoupleDataAndTarget(_conFeatureSets, _conLabels):
    conCoupled = {}
    for doc_id in _conFeatureSets:
        if _conLabels != None:
            conCoupled[doc_id] = (_conFeatureSets[doc_id], _conLabels[doc_id])
        else:
            conCoupled[doc_id] = (_conFeatureSets[doc_id], None)
    return conCoupled

def SplitTrainingSet(_conDocs, _conLabels, _nTrainingFolds, _fRatioHoldOverForTest):
    conExecutions = []

    # we want to populate conExecutions such that each entry is a set of training examples paired with a set of test examples
    nDocs = len(_conDocs)
    nTestDocuments = math.floor(nDocs * _fRatioHoldOverForTest)

    for k in range(0, int(_nTrainingFolds)):
        conDocsForSelection = _conDocs.copy()
        conTestDocuments = []
        for test_doc_index in range(1, nTestDocuments):
            test_document_id = random.choice(list(conDocsForSelection))
            conDocsForSelection.pop(test_document_id)
            conTestDocuments.append(test_document_id)

        conTrainingDocuments = []
        nTrainingDocumentsAdded = 0;
        for training_doc in conDocsForSelection:
            if training_doc in conTestDocuments:
                continue
            else:
                conTrainingDocuments.append(training_doc)
                nTrainingDocumentsAdded = nTrainingDocumentsAdded + 1

        conExecutions.append((conTestDocuments, conTrainingDocuments))

        if debug_traces:
            print("Test Documents for execution", k, conTestDocuments)
            print("Training Docs for execution ", k, conTrainingDocuments)
    return conExecutions

def GetVocab(_conTrainingSet, _conDocuments):

    conVocab = defaultdict(int)
    conBigrams = set()
    conBigramCount = {}
    for doc_id in _conTrainingSet:
        words = _conDocuments[doc_id].split()
        for word in words:
            conVocab[word] += 1
    return conVocab

    #     finder = BigramCollocationFinder.from_words(words)
    #     conNgrams = finder.ngram_fd
    #     for ngram in conNgrams:
    #         if ngram in conBigramCount:
    #             conBigramCount[ngram] += 1
    #         else:
    #             conBigramCount[ngram] = 1
    #
    # for idx in conBigramCount:
    #     if conBigramCount[idx] < 2:
    #         conBigrams.add(idx)
    # return conVocab, conBigrams

def TrainNB(_conDocuments, _conTrainingSet, _conLabels):
    conLogLikelihoodsForTruthful = {}
    conLogLikelihoodsForFalsified = {}
    conVocab = set()

    conTruthfulWordCounts, conFalsifiedWordCounts = CountWords(_conDocuments, _conTrainingSet, _conLabels);
    # conNegativeWordCounts, nNegativeWordCount = CountWords(_strNegativeFilename, _conStopwords);

    nTruthfulWordCount = sum(conTruthfulWordCounts.values())
    nFalsifiedWordCount = sum(conFalsifiedWordCounts.values())

    conVocab = set.union(set(conTruthfulWordCounts.keys()), set(conFalsifiedWordCounts.keys()))

    nVocabSize = len(conVocab)

    if 0:
        print("Truthful word counts:", nTruthfulWordCount, conTruthfulWordCounts)
        print("Falsified word counts:", nFalsifiedWordCount, conFalsifiedWordCounts)
        print("Vocabulary:", conVocab)

    for word in conVocab:
        count = 0
        if word in conTruthfulWordCounts:
            count = conTruthfulWordCounts[word]
        # print("for", word, "count:", count)
        # divide the count of this word in this category +1 - by the count in this category, of all words in the vocabulary + Vocab size
        likelihood = (count + 1) / (nTruthfulWordCount + nVocabSize)  # check the vocab size
        conLogLikelihoodsForTruthful[word] = math.log(likelihood)

    for word in conVocab:
        count = 0
        if word in conFalsifiedWordCounts:
            count = conFalsifiedWordCounts[word]
        # divide the count of this word in this category +1 - by the count in this category, of all words in the vocabulary + Vocab size
        likelihood = (count + 1) / (nFalsifiedWordCount + nVocabSize)
        conLogLikelihoodsForFalsified[word] = math.log(likelihood)

    if 0:
        conSortedTruthful = sorted(conLogLikelihoodsForTruthful, key=conLogLikelihoodsForTruthful.get, reverse=True)
        conSortedFalsified = sorted(conLogLikelihoodsForFalsified, key=conLogLikelihoodsForFalsified.get, reverse=True)
        print("Top words for truthful reviews:",
              conSortedTruthful);
        print("Top words for falsified reviews:",
              conSortedFalsified);

    return conVocab, conLogLikelihoodsForTruthful, conLogLikelihoodsForFalsified


def Classify(_conVocab, _logLikelihoodForTruthful, _logLikelihoodForFalsified, _docTest):
    nSumTruthful = 0;
    nSumFalsified = 0;
    words = _docTest.split()
    for word in words:
        if word in _conVocab:
            nSumTruthful = nSumTruthful + _logLikelihoodForTruthful[word]
            nSumFalsified = nSumFalsified + _logLikelihoodForFalsified[word]

    classification = Classification;
    nRatio = 0.0
    if nSumTruthful > nSumFalsified:
        classification = Classification.T;
        nRatio = nSumTruthful / nSumFalsified
    else:
        classification = Classification.F;
        nRatio = nSumFalsified / nSumTruthful

    return classification, nRatio


def ExtractSentimentWords(_strSentimentFilename):
    conSentimentWords = set()

    if os.path.isfile(_strSentimentFilename):
        with open(_strSentimentFilename) as fileSentiment:
            content = fileSentiment.readlines()
            for line in content:
                conSentimentWords.add(line.strip())

        fileSentiment.close()

    return conSentimentWords


# Extract all data from the training set.
def ExtractDocuments(_strTruthfulFilename, _strFalsifiedFilename, _conStopWords):
    conDocuments = {}
    conDocumentsProcessed = {}
    conLabels = {}

    # Experiment with binaryNB
    binaryNB = False

    if os.path.isfile(_strTruthfulFilename):
        # Necessary to open the file like this, to avoid crashes on processing the file.
        with codecs.open(_strTruthfulFilename, encoding='utf-8') as fileTrueExamples:
            content = fileTrueExamples.readlines()
            for line in content:
                review_id, review_body_pure, review_body = PreProcessDoc(line, _conStopWords)
                if not binaryNB:
                    conDocuments[review_id] = review_body_pure
                    conDocumentsProcessed[review_id] = review_body
                else:
                    #Note processed version only is done here - this section needs update.
                    words = review_body
                    " ".join(sorted(set(words), key=words.index))
                    conDocuments[review_id] = words
                    # print(words)

                conLabels[review_id] = Classification.T
        fileTrueExamples.close()

    if os.path.isfile(_strFalsifiedFilename):
        # Necessary to open the file like this, to avoid crashes on processing the file.
        with codecs.open(_strFalsifiedFilename, encoding='utf-8') as fileFalseExamples:
            content = fileFalseExamples.readlines()

            for line in content:
                review_id, review_body_pure, review_body = PreProcessDoc(line, _conStopWords)
                conDocuments[review_id] = review_body_pure
                conDocumentsProcessed[review_id] = review_body
                conLabels[review_id] = Classification.F
            #finder = BigramCollocationFinder.from_words(conDocuments.split())
        fileFalseExamples.close()

    return conDocuments, conDocumentsProcessed, conLabels


def ReadTestSet(_strTestFilename, _conStopWords):
    conTestSet = {}
    conTestSetProcessed = {}
    if os.path.isfile(_strTestFilename):
        # Necessary to open the file like this, to avoid crashes on processing the file.
        with codecs.open(_strTestFilename, encoding='utf-8') as fileTestSet:
            content = fileTestSet.readlines()
            for line in content:
                review_id, review_body_pure, review_body = PreProcessDoc(line, _conStopWords)

                # print(review_id, body)
                conTestSet[review_id] = review_body_pure
                conTestSetProcessed[review_id] = review_body

            fileTestSet.close()
    return conTestSet, conTestSetProcessed


def CountPOS(_conDocuments):#, _conLabels):
    conPosCounts = {}

    #countTrue = Counter()
    #countFalse = Counter()

    for doc_id in _conDocuments:
        # print(doc_id, _conLabels[doc_id])  # , _conDocuments[doc_id])
        conCounts = Counter()
        conCounts.clear()
        # print(doc_id)
        # sentences = nltk.sent_tokenize(_conDocuments[doc_id])
        # for sentence in sentences:
        # words = nltk.word_tokenize(sentence)
        words = nltk.word_tokenize(_conDocuments[doc_id])
        # tagset = 'universal'
        # conPOS = nltk.tag._pos_tag(words, tagset, tagger)

        # conPosTags = nltk.batch_pos_tag(words, tagset)
        conPosTags = tagger.tag(words)
        conCounts = Counter(elem[1] for elem in conPosTags)
        conPosCounts[doc_id] = conCounts
        # nVerb = conCounts['VERB']
        # nPronoun = conCounts['PRON']
        # fRatioVerb = nVerb / sum(conCounts.values())
        # fRatioPron = nPronoun / sum(conCounts.values())
        # print(doc_id, conCounts, 'Ratio of verbs', fRatioVerb, 'Ratio of pronouns', fRatioPron)



        # X = numpy.arange(len(conCounts))
        # fig = pyplot.figure()
        # pyplot.bar(X, conCounts.values(), align='center')
        # pyplot.xticks(X, conCounts.keys(), rotation='vertical')
        # szTitle = doc_id + str(_conLabels[doc_id])
        # pyplot.title(szTitle)

        # szName = str(doc_id) + '.png'
        # pyplot.savefig(szName)
        # pyplot.close()
    # fMeanVerbRatio = sum(conAllVerbRatio.values()) / len(conAllVerbRatio)
    # fMeanPronRatio = sum(conAllPronounRatio.values()) / len(conAllPronounRatio)
    # print('Average verb ratio:', fMeanVerbRatio)
    # print('Average pron ratio:', fMeanPronRatio)
    # nCountVerbRuleSuccess = 0;
    # nCountPronRuleSuccess = 0;

    #
    #
    # print('verb success:', nCountVerbRuleSuccess / len(_conTrainingSet))
    # print('pron success:', nCountPronRuleSuccess / len(_conTrainingSet))

        # if _conLabels[doc_id] == Classification.F:
        #     countFalse += conPosCounts[doc_id]
        # else:
        #     countTrue += conPosCounts[doc_id]


    # X = np.arange(len(countTrue))
    # fig = pyplot.figure()
    # pyplot.bar(X, countTrue.values(), align='center')
    # pyplot.xticks(X, countTrue.keys(), rotation='vertical')
    # szTitle = "Truthful"
    # pyplot.title(szTitle)
    # szName = 'truthful.png'
    # pyplot.savefig(szName)
    # pyplot.close()
    #
    # X = np.arange(len(countFalse))
    # fig = pyplot.figure()
    # pyplot.bar(X, countFalse.values(), align='center')
    # pyplot.xticks(X, countFalse.keys(), rotation='vertical')
    # szTitle = "Falsified"
    # pyplot.title(szTitle)
    # szName = 'falsified.png'
    # pyplot.savefig(szName)
    # pyplot.close()

    return conPosCounts


from itertools import groupby


def yule(_entry):
    # yule's I measure (the inverse of yule's K measure)
    # higher number is higher diversity - richer vocabulary
    d = {}
    stemmer = PorterStemmer()
    for w in _entry:
        try:
            d[w] += 1
        except KeyError:
            d[w] = 1

    M1 = float(len(d))
    M2 = sum([len(list(g)) * (freq ** 2) for freq, g in groupby(sorted(d.values()))])

    try:
        return (M1 * M1) / (M2 - M1)
    except ZeroDivisionError:
        return 0

# want to get counts for each word, for each category, storing each in a dictionary lookup.
# this count can then be smoothed, and have the probability calculated.
def CountWords(_conDocuments, _conTrainingSet, _conLabels):
    if debug_traces:
        print("Counting words in training set...")

    conCountTruthful = {}
    conCountFalsified = {}

    for document in _conTrainingSet:
        words = _conDocuments[document].split()
        documentClassification = _conLabels[document]
        if documentClassification == Classification.T:
            for word in words:
                if word in conCountTruthful:
                    conCountTruthful[word] = conCountTruthful[word] + 1
                else:
                    conCountTruthful[word] = 1
        else:
            for word in words:
                if word in conCountFalsified:
                    conCountFalsified[word] = conCountFalsified[word] + 1
                else:
                    conCountFalsified[word] = 1

    return conCountTruthful, conCountFalsified


def PreProcessDoc(_document, _conStopWords):
    # need to fully verify this regexp - might need to capture the doc id which we are subbing for ''
    # also, should precompile the regexp since this is a main loop
    review_id = re.match('^\s*\S+', _document).group()
    review_body = unicodedata.normalize('NFD',
                                        _document)  # get rid of silly unicode characters - not sure of the best way to do this, but this works, although it sort of corrupts the word too

    review_body = re.sub('^\s*\S+\W*', '', review_body)  # remove the doc id and leading whitespace

    review_body_pure = review_body # return a pure form (unprocessed)
    review_body = review_body.lower()  # unify to lower case - I don't see any need to keep case.
    review_body = re.sub('[\W+]', ' ', review_body)  # remove everything that isn't a ws or alphanum
    # review_body = re.sub(r'\b(?:not|never|no)\b[\w\s]+[^\w\s]', Negate, review_body)

    #review_body = Stem(review_body)
    review_body = RemoveSmallWords(review_body, 2)

    # Remove stop words
    review_body = ' '.join([i for i in review_body.split() if i not in _conStopWords])

    return review_id, review_body_pure, review_body,


def Stem(_document):
    stemmer = PorterStemmer()
    conWords = _document.split()
    conStemmedWords = []
    for word in conWords:
        strWordStemmed = stemmer.stem(word, 0, len(word) - 1)
        conStemmedWords.append(strWordStemmed)

    strStemmed = " ".join(conStemmedWords)
    return (strStemmed)


def Negate(_match):
    strMatch = _match.group().split()
    conWords = []
    for word in strMatch:
        strNegWord = 'NOT_' + word
        conWords.append(strNegWord)

    strNegation = " ".join(conWords)
    return strNegation


# Remove words of less than minSize characters
def RemoveSmallWords(_str, _nMinSize):
    output = ""
    output = ' '.join(word for word in _str.split() if len(word) >= _nMinSize)

    return output


def ReadStopWords(_strStopwordsFilename):
    conStopwords = set()
    if _strStopwordsFilename != "" and os.path.isfile(_strStopwordsFilename):
        with open(_strStopwordsFilename) as fileStop:
            content = fileStop.readlines()
            for line in content:
                word = re.sub('[\s]', '', line)
                conStopwords.add(word)
            fileStop.close()

    # print('Stopwords:\n', conStopwords,'\n')
    return conStopwords


class Classification(Enum):
    T = 1
    F = -1


# The following stemming code is from:
# http://tartarus.org/martin/PorterStemmer/

# !/usr/bin/env python

"""Porter Stemming Algorithm
This is the Porter stemming algorithm, ported to Python from the
version coded up in ANSI C by the author. It may be be regarded
as canonical, in that it follows the algorithm presented in

Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
no. 3, pp 130-137,

only differing from it at the points maked --DEPARTURE-- below.

See also http://www.tartarus.org/~martin/PorterStemmer

The algorithm as described in the paper could be exactly replicated
by adjusting the points of DEPARTURE, but this is barely necessary,
because (a) the points of DEPARTURE are definitely improvements, and
(b) no encoding of the Porter stemmer I have seen is anything like
as exact as this version, even with the points of DEPARTURE!

Vivake Gupta (v@nano.com)

Release 1: January 2001

Further adjustments by Santiago Bruno (bananabruno@gmail.com)
to allow word input not restricted to one word per line, leading
to:

release 2: July 2008
"""

import sys


class PorterStemmer:
    def __init__(self):
        """The main part of the stemming algorithm starts here.
        b is a buffer holding a word to be stemmed. The letters are in b[k0],
        b[k0+1] ... ending at b[k]. In fact k0 = 0 in this demo program. k is
        readjusted downwards as the stemming progresses. Zero termination is
        not in fact used in the algorithm.

        Note that only lower case sequences are stemmed. Forcing to lower case
        should be done before stem(...) is called.
        """

        self.b = ""  # buffer for word to be stemmed
        self.k = 0
        self.k0 = 0
        self.j = 0  # j is a general offset into the string

    def cons(self, i):
        """cons(i) is TRUE <=> b[i] is a consonant."""
        if self.b[i] == 'a' or self.b[i] == 'e' or self.b[i] == 'i' or self.b[i] == 'o' or self.b[i] == 'u':
            return 0
        if self.b[i] == 'y':
            if i == self.k0:
                return 1
            else:
                return (not self.cons(i - 1))
        return 1

    def m(self):
        """m() measures the number of consonant sequences between k0 and j.
        if c is a consonant sequence and v a vowel sequence, and <..>
        indicates arbitrary presence,

           <c><v>       gives 0
           <c>vc<v>     gives 1
           <c>vcvc<v>   gives 2
           <c>vcvcvc<v> gives 3
           ....
        """
        n = 0
        i = self.k0
        while 1:
            if i > self.j:
                return n
            if not self.cons(i):
                break
            i = i + 1
        i = i + 1
        while 1:
            while 1:
                if i > self.j:
                    return n
                if self.cons(i):
                    break
                i = i + 1
            i = i + 1
            n = n + 1
            while 1:
                if i > self.j:
                    return n
                if not self.cons(i):
                    break
                i = i + 1
            i = i + 1

    def vowelinstem(self):
        """vowelinstem() is TRUE <=> k0,...j contains a vowel"""
        for i in range(self.k0, self.j + 1):
            if not self.cons(i):
                return 1
        return 0

    def doublec(self, j):
        """doublec(j) is TRUE <=> j,(j-1) contain a double consonant."""
        if j < (self.k0 + 1):
            return 0
        if (self.b[j] != self.b[j - 1]):
            return 0
        return self.cons(j)

    def cvc(self, i):
        """cvc(i) is TRUE <=> i-2,i-1,i has the form consonant - vowel - consonant
        and also if the second c is not w,x or y. this is used when trying to
        restore an e at the end of a short  e.g.

           cav(e), lov(e), hop(e), crim(e), but
           snow, box, tray.
        """
        if i < (self.k0 + 2) or not self.cons(i) or self.cons(i - 1) or not self.cons(i - 2):
            return 0
        ch = self.b[i]
        if ch == 'w' or ch == 'x' or ch == 'y':
            return 0
        return 1

    def ends(self, s):
        """ends(s) is TRUE <=> k0,...k ends with the string s."""
        length = len(s)
        if s[length - 1] != self.b[self.k]:  # tiny speed-up
            return 0
        if length > (self.k - self.k0 + 1):
            return 0
        if self.b[self.k - length + 1:self.k + 1] != s:
            return 0
        self.j = self.k - length
        return 1

    def setto(self, s):
        """setto(s) sets (j+1),...k to the characters in the string s, readjusting k."""
        length = len(s)
        self.b = self.b[:self.j + 1] + s + self.b[self.j + length + 1:]
        self.k = self.j + length

    def r(self, s):
        """r(s) is used further down."""
        if self.m() > 0:
            self.setto(s)

    def step1ab(self):
        """step1ab() gets rid of plurals and -ed or -ing. e.g.

           caresses  ->  caress
           ponies    ->  poni
           ties      ->  ti
           caress    ->  caress
           cats      ->  cat

           feed      ->  feed
           agreed    ->  agree
           disabled  ->  disable

           matting   ->  mat
           mating    ->  mate
           meeting   ->  meet
           milling   ->  mill
           messing   ->  mess

           meetings  ->  meet
        """
        if self.b[self.k] == 's':
            if self.ends("sses"):
                self.k = self.k - 2
            elif self.ends("ies"):
                self.setto("i")
            elif self.b[self.k - 1] != 's':
                self.k = self.k - 1
        if self.ends("eed"):
            if self.m() > 0:
                self.k = self.k - 1
        elif (self.ends("ed") or self.ends("ing")) and self.vowelinstem():
            self.k = self.j
            if self.ends("at"):
                self.setto("ate")
            elif self.ends("bl"):
                self.setto("ble")
            elif self.ends("iz"):
                self.setto("ize")
            elif self.doublec(self.k):
                self.k = self.k - 1
                ch = self.b[self.k]
                if ch == 'l' or ch == 's' or ch == 'z':
                    self.k = self.k + 1
            elif (self.m() == 1 and self.cvc(self.k)):
                self.setto("e")

    def step1c(self):
        """step1c() turns terminal y to i when there is another vowel in the stem."""
        if (self.ends("y") and self.vowelinstem()):
            self.b = self.b[:self.k] + 'i' + self.b[self.k + 1:]

    def step2(self):
        """step2() maps double suffices to single ones.
        so -ization ( = -ize plus -ation) maps to -ize etc. note that the
        string before the suffix must give m() > 0.
        """
        if self.b[self.k - 1] == 'a':
            if self.ends("ational"):
                self.r("ate")
            elif self.ends("tional"):
                self.r("tion")
        elif self.b[self.k - 1] == 'c':
            if self.ends("enci"):
                self.r("ence")
            elif self.ends("anci"):
                self.r("ance")
        elif self.b[self.k - 1] == 'e':
            if self.ends("izer"):      self.r("ize")
        elif self.b[self.k - 1] == 'l':
            if self.ends("bli"):
                self.r("ble")  # --DEPARTURE--
            # To match the published algorithm, replace this phrase with
            #   if self.ends("abli"):      self.r("able")
            elif self.ends("alli"):
                self.r("al")
            elif self.ends("entli"):
                self.r("ent")
            elif self.ends("eli"):
                self.r("e")
            elif self.ends("ousli"):
                self.r("ous")
        elif self.b[self.k - 1] == 'o':
            if self.ends("ization"):
                self.r("ize")
            elif self.ends("ation"):
                self.r("ate")
            elif self.ends("ator"):
                self.r("ate")
        elif self.b[self.k - 1] == 's':
            if self.ends("alism"):
                self.r("al")
            elif self.ends("iveness"):
                self.r("ive")
            elif self.ends("fulness"):
                self.r("ful")
            elif self.ends("ousness"):
                self.r("ous")
        elif self.b[self.k - 1] == 't':
            if self.ends("aliti"):
                self.r("al")
            elif self.ends("iviti"):
                self.r("ive")
            elif self.ends("biliti"):
                self.r("ble")
        elif self.b[self.k - 1] == 'g':  # --DEPARTURE--
            if self.ends("logi"):      self.r("log")
            # To match the published algorithm, delete this phrase

    def step3(self):
        """step3() dels with -ic-, -full, -ness etc. similar strategy to step2."""
        if self.b[self.k] == 'e':
            if self.ends("icate"):
                self.r("ic")
            elif self.ends("ative"):
                self.r("")
            elif self.ends("alize"):
                self.r("al")
        elif self.b[self.k] == 'i':
            if self.ends("iciti"):     self.r("ic")
        elif self.b[self.k] == 'l':
            if self.ends("ical"):
                self.r("ic")
            elif self.ends("ful"):
                self.r("")
        elif self.b[self.k] == 's':
            if self.ends("ness"):      self.r("")

    def step4(self):
        """step4() takes off -ant, -ence etc., in context <c>vcvc<v>."""
        if self.b[self.k - 1] == 'a':
            if self.ends("al"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'c':
            if self.ends("ance"):
                pass
            elif self.ends("ence"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'e':
            if self.ends("er"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'i':
            if self.ends("ic"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'l':
            if self.ends("able"):
                pass
            elif self.ends("ible"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'n':
            if self.ends("ant"):
                pass
            elif self.ends("ement"):
                pass
            elif self.ends("ment"):
                pass
            elif self.ends("ent"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'o':
            if self.ends("ion") and (self.b[self.j] == 's' or self.b[self.j] == 't'):
                pass
            elif self.ends("ou"):
                pass
            # takes care of -ous
            else:
                return
        elif self.b[self.k - 1] == 's':
            if self.ends("ism"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 't':
            if self.ends("ate"):
                pass
            elif self.ends("iti"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'u':
            if self.ends("ous"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'v':
            if self.ends("ive"):
                pass
            else:
                return
        elif self.b[self.k - 1] == 'z':
            if self.ends("ize"):
                pass
            else:
                return
        else:
            return
        if self.m() > 1:
            self.k = self.j

    def step5(self):
        """step5() removes a final -e if m() > 1, and changes -ll to -l if
        m() > 1.
        """
        self.j = self.k
        if self.b[self.k] == 'e':
            a = self.m()
            if a > 1 or (a == 1 and not self.cvc(self.k - 1)):
                self.k = self.k - 1
        if self.b[self.k] == 'l' and self.doublec(self.k) and self.m() > 1:
            self.k = self.k - 1

    def stem(self, p, i, j):
        """In stem(p,i,j), p is a char pointer, and the string to be stemmed
        is from p[i] to p[j] inclusive. Typically i is zero and j is the
        offset to the last character of a string, (p[j+1] == '\0'). The
        stemmer adjusts the characters p[i] ... p[j] and returns the new
        end-point of the string, k. Stemming never increases word length, so
        i <= k <= j. To turn the stemmer into a module, declare 'stem' as
        extern, and delete the remainder of this file.
        """
        # copy the parameters into statics
        self.b = p
        self.k = j
        self.k0 = i
        if self.k <= self.k0 + 1:
            return self.b  # --DEPARTURE--

        # With this line, strings of length 1 or 2 don't go through the
        # stemming process, although no mention is made of this in the
        # published algorithm. Remove the line to match the published
        # algorithm.

        self.step1ab()
        self.step1c()
        self.step2()
        self.step3()
        self.step4()
        self.step5()
        return self.b[self.k0:self.k + 1]


if __name__ == "__main__":
    print("Running Deception Detection")
    main(sys.argv[1:])
