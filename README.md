#Deception Detection
##CSCI5832 Assignment 3
###Geoffrey Sutcliffe
#####Available @ https://bitbucket.org/geoff_sutcliffe/deception

###Instructions:
The program operates in one of two modes - **TEST** or **DEV**. Furthermore, the previous **NB** implementation can be used, instead of the new implementation. It can also be run in **POS CACHING** mode.
####TEST
Test on the training set, using the entire training set for training. This mode is selected by supplying a TEST_SET with the -t parameter. 

Test mode can be executed using either the old NB implementation (with -nb flag), or the new implementation. 

####DEV
Perform cross validation on the specified training set. A number can be specified with -x, which is the number of cross validation folds (number of times to split the training set). The second number to be specified, with -j, is the TEST_RATIO, which is the ratio of the training set to hold over for testing in each fold.

The way this works is;
  
    for 1 .. X_VALIDATION_FOLDS
		randomly select TEST_RATIO training examples to use for testing.
		use the remaining training examples for training.      

If parameters are not supplied, 30% of the set is held over for testing, and 10 cross-validation folds are performed.

Dev mode can be executed using either the old NB implementation (with -nb flag), or the new implementation. 

####Usage:

>usage: sutcliffe-geoffrey-assgn3.py [-h] -t TRUTHFUL\_TRAINING\_SET -f
>                                    FALSIFIED\_TRAINING\_SET
>                                    [-x X_VALIDATION\_FOLDS] [-j TEST\_RATIO]
>                                    [-tt TEST\_SET] [-d] [-s STOPWORD] [-p]
>                                    [-c] [-nb]


    -p Specify the filename for positive sentiment examples
	-n Specify the filename for positive sentiment examples

####Optional Parameters:

	-t Specify the testset filename
	-p Use preprocessed Parts of Speech file (to speed up performance)
	-c Create Parts of Speech file (to speed up performance)
	-nb Run in Naive Bayes (old) mode using only lexical cues.
	-x Number of Cross Validation folds
	-j Ratio of examples to hold out for testing in each iteration 
	-s Specify the filename for Stopword list (text file with one word per line)
	-d Print out debug traces
	-h Display options

i.e. to run with test set to check results:
	
	python3 sutcliffe-geoffrey-assgn3.py -p hotelT-train.txt -n hotelF-train.txt -t HW3-testset.txt -s stopwords.txt

It is recommended to run once in POS creation mode, to the extract the parts of speech for the training set and the test set:
	
	python3 sutcliffe-geoffrey-assgn3.py -p hotelT-train.txt -n hotelF-train.txt -t HW3-testset.txt -c

and then run again to classify using the created POS file, including stopwords list
	
	python3 sutcliffe-geoffrey-assgn3.py -p hotelT-train.txt -n hotelF-train.txt -t HW3-testset.txt -p -s stopwords.txt

Finally, to execute on the old NB model (Does not need stopwords list, or POS creation/usage)

	python3 sutcliffe-geoffrey-assgn3.py -p hotelT-train.txt -n hotelF-train.txt -t HW3-testset.txt -nb

To run in development mode on a previously created POS file
	
	python3 sutcliffe-geoffrey-assgn3.py -p hotelT-train.txt -n hotelF-train.txt -p -s stopwords.txt


##Basic Implementation
I first tried to use the Naïve Bayes classifier using unigrams, as per the previous project, but changing the output class to Truthful or Falsified. 
Performance was measured using the same framework as previously – 10x cross validation of the training set. I did not move it over to the existing NLTK framework, as the current framework is operating OK.
The performance was equivalent to chance – ~50%. The meaning of this figure is that words alone are not a good discriminant. 

## Improved System Results and Description

Many different solutions were researched and implemented – the efforts are described below.
The best result achieved was ~73% with an average of ~60% correct classification 
>Mean score: 0.6039682539682539 Variance: 0.006310783572688335 , Worst Score: 0.4444444444444444 for test set 7 , Best Score: 0.7301587301587301 for test set 2

There is a large degree of variance on repeated executions, based on the training set. I estimate 56% on the test set - I am not sure that the larger training corpus is going to make things much better! 


Below is described the implementation that gave these results, as well as other trials that were performed but that did not improve, or reduced, performance.

#####Classifier
The classifier is a Support Vector Machine with an ‘RBF’ kernel. All features are scaled prior to training the model. It is entirely the NLTK/scikit-learn implementation, so training data is fed in as arrays of features and target classes.

#####Preprocessing
Preprocessing is as in the previous project, with some minor changes:

- Words are no longer stemmed. Worse results were observed with stemming. This is likely due to many of the stylometric features no longer occurring after a word is stemmed.
- Small words removal changed from 3 to 2 (remove all words less than 2 characters)
- Negation is no longer detected and written into the corpus. I saw no reference material that suggested negations were higher or lower in frequency in deceptive writing.
- Preprocessing now returns both a processed version, and the original version of the document - some features require the original document (for example, count of punctuation characters, count of uppercase)
- Stop words are removed

The full preprocessing that occurs is:

- Unicode normalization,
- Remove the document id and leading whitespace
- Casefold to lower case
- Remove punctuation
- Remove small words
- Remove stop words

#####Features
Features are of several types:

######Character Level

- Count of Punctuation characters.
- Ratio of Uppercase characters to total number of characters

######Word Level

- Count of Sentiment Words (from https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html#datasets)
	- This is counted separately for positive and negative sentiment words. In fact, it was found that it was better to only use the positive sentiment word count feature. This may show that the nature of positive deceptive writing is more lexically effected than negative deceptive writing.
- Number of Hapax Legomena words – those that occur in the training set only once. **[Shojaee2013]**. Counting Dislegomena did not seem to have a positive effect

######Parts of Speech Level
- Count of Past Tense Verbs (VBD + VBN) (based on the intuition of describing some actual activity the reviewer has actually done – also Feng2012)
- Count of Pronouns (PRP + PRP$)
- Count of Adjectives (JJ + JJR + JJS)

The primary motivation for this approach comes from **[Shojaee2013]**, which proposes primarily stylometric cues as the best way to determine deceptive text – with the assertion that one’s writing style changes when writing deceptively.
As noted, this classifier and set of features gave the best observed results, but the results in **[Shojaee2013]** and **[Ott11]** were far greater (84%). The reason for the discrepancy is likely due to the nature of the datasets - perhaps the truthful and deceptive reviews have some other latent factor that is being detected by the selected features.

I have tried several configurations of the ‘C’ and ‘gamma’ values, and several different kernels.

Another alternative cause of the poor performance is voiced in **[Hancock2013]**, which states “our results show that classifiers trained and tests on reviews of different sentiments perform worse, despite having more training data. To use this concept it may be worth adding a layer of sentiment classification (using the training set from the previous project), to classify within a 90% accuracy, the sentiment of the truthful/deceptive reviews, and then train a single class SVM (typically used to detect outliers) to detect the deceptive reviews. I tried to prove this concept by creating a dataset of only negative deceptive and negative truthful reviews – but results did not improve. The use of negative/positive sentiment words is a reasonable proxy for this functionality, having not added this functionality.

Although I have included features from the POS category, I analysed the POS tags in the below charts from the truthful and falsified training data:

![](https://bytebucket.org/geoff_sutcliffe/deception/raw/3300b77c5a8c924fc6e1b6800616732494ea59a5/img/falsified.png) 
![](https://bytebucket.org/geoff_sutcliffe/deception/raw/3300b77c5a8c924fc6e1b6800616732494ea59a5/img/truthful.png) 

It is not easy to spot any obvious discrepancies in the POS counts for the training corpus - at least not to make any easy decisions about classification features.

##Other (failed) attempts
In addition to tuning the SVM classifier (which had minimal effect), many more features were experimented with, inspired by a multitude of articles on deception detection.
######Counts and ratios of the following Parts of Speech:
- Verbs. Based on **[Feng2012]** which says that deceptive reviews tend to use verbs more often.
- Nouns. Based on several papers that reference the increased number of spatial details in truthful reviews **[Feng2012]**. 
- ProperNouns.
- Determiners
- Adverbs
- Prepositions
- Ratio of Hapax Dislegomena words – those that occur in the training set only once. **[Shojaee2013]**
- Unigrams (count of each word in the training corpus, as it appears in the test document) – huge featureset
- Bigrams (count of each bigram in the training corpus that occurs more than once) – similar principle to Hapax Legomena
- Count of spaces
- Count of Digits/Letters
- Count of short words (< 3 char)
- Count of words
- Ratio of word types to word tokens (how rich is the vocabulary)

##Further work
**[Li2014]** – The next thing to try would be the SAGE (Sparse Additive Generative) model proposed in [Eisenstein2011], modified by Li et al, which is an LDA-like approach, but uses a Laplacian prior rather than a Dirichlet prior. Although this seems like a more theoretically sound, in the way it models the contribution of latent aspects of the review writer, it is a more complicated model so has not been implemented yet.

**[Shojaee2013]** also suggests counting ‘function words’, ~100 of which are listed in Zheng2005

##References
**[Shojaee2013]** Shojaee, S., Murad, M. A. A., Azman, A. Bin, Sharef, N. M., & Nadali, S. (2013). Detecting deceptive reviews using lexical and syntactic features. 2013 13th International Conference on Intellient Systems Design and Applications, 53–58. http://doi.org/10.1109/ISDA.2013.6920707

**[Feng2012]** Feng, S., Banerjee, R., & Choi, Y. (2012). Syntactic stylometry for deception detection. Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics: Short Papers-Volume 2. Association for Computational Linguistics, (July), 171–175.

**[Eisenstein2011]** Eisenstein, J., Ahmed, A., & Xing, E. P. E. (2011). Sparse additive generative models of text. Proceedings of the 28th International Conference on Machine Learning (ICML-11), 1041–1048. http://doi.org/10.1.1.206.5167

**[Li2014]** Li, J., Ott, M., Cardie, C., & Hovy, E. (2014). Towards a General Rule for Identifying Deceptive Opinion Spam. Acl-2014, 1566–1576.

**[Hancock2012]** Hancock, J. T. (2013). Negative Deceptive Opinion Spam. Naacl, (June), 497–501.

**[Ott2011]** Ott, M., Choi, Y., Cardie, C., & Hancock, J. T. (2011). Finding Deceptive Opinion Spam by Any Stretch of the Imagination, 11. Retrieved from http://arxiv.org/abs/1107.4557



